// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { LineHintsComponent } from './line-hints.component';

@NgModule({
    imports: [

    ],
    declarations: [
        LineHintsComponent,
    ],
    exports: [
        LineHintsComponent,
    ]
})
export class LineHintsModule {

}
